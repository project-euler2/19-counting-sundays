import datetime

def count_days(begin_date, end_date,search_day):
    begin_date = datetime.datetime(begin_date[2],begin_date[1],begin_date[0])
    end_date = datetime.datetime(end_date[2],end_date[1],end_date[0])
    num_of_sundays = 0
    while begin_date != end_date:
        if begin_date.weekday() == search_day and begin_date.day == 1 :
            num_of_sundays += 1
        begin_date += datetime.timedelta(days = 1)
    return num_of_sundays

print(count_days([1,1,1901],[31,12,2000],6))